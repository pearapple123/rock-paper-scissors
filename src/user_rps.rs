use std::io;
use super::RpsChoice;

pub fn user_input_to_enum() -> Result<RpsChoice, io::Error> {
    println!("Rock, paper or scissors! Type r, p or s below");

    loop {
        let mut input = String::new();

        io::stdin().read_line(&mut input)?;

        let input = input.trim();

        return match input {
            "r" => Ok(RpsChoice::Rock),
            "p" => Ok(RpsChoice::Paper),
            "s" => Ok(RpsChoice::Scissors),
            _ => {
                    println!("Type r,p or s only");
                    continue;
            }
        }

    }
}

