use rand::Rng;
use super::RpsChoice;

pub fn rand_int_to_enum() -> RpsChoice {
    let mut rng = rand::thread_rng();

    let ri: u8 = rng.gen_range(0..=2);
    match ri {
        0 => RpsChoice::Rock,
        1 => RpsChoice::Paper,
        _ => RpsChoice::Scissors
    }
}
