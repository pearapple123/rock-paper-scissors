use std::io;
use std::error::Error;
use std::default::Default;
use rock_paper_scissors::{user_rps, 
comp_rps,
GameResult,
SessionRecord
};

fn game() -> Result<GameResult, io::Error> {
    let user_input = user_rps::user_input_to_enum()?;
    let comp_input = comp_rps::rand_int_to_enum();
    let game_result = GameResult::new(&user_input, &comp_input);

    println!("You threw {user_input:?} and the computer threw {comp_input:?}\n");

    Ok(game_result)
}

// Game loop
fn main() -> Result<(), Box<dyn Error>> {
    let mut user_record: SessionRecord = Default::default();

    loop {
        let game_result = game()?;

        user_record.update_record(&game_result);

        println!("Result: {game_result:?}");
        println!("Current record: {user_record}\n")
    }

}
