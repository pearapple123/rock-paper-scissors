use std::cmp::{Ordering, Ord};
use std::fmt;
use std::default::Default;

pub mod user_rps;
pub mod comp_rps;

#[derive(Debug, Eq, PartialEq, PartialOrd)]
pub enum RpsChoice {
    Rock,
    Paper,
    Scissors
}

impl Ord for RpsChoice {
    fn cmp(&self, other: &RpsChoice) -> Ordering {
        match (self, other) {
            (RpsChoice::Rock, RpsChoice::Scissors) => Ordering::Greater,
            (RpsChoice::Scissors, RpsChoice::Paper) => Ordering::Greater,
            (RpsChoice::Paper, RpsChoice::Rock) => Ordering::Greater,
            (RpsChoice::Rock, RpsChoice::Rock) => Ordering::Equal,
            (RpsChoice::Paper, RpsChoice::Paper) => Ordering::Equal,
            (RpsChoice::Scissors, RpsChoice::Scissors) => Ordering::Equal,
            _ => Ordering::Less
        }
    }
}

#[derive(Debug)]
pub enum GameResult {
    Victory,
    Draw,
    Loss
}

impl GameResult {
    pub fn new(player: &RpsChoice, comp: &RpsChoice) -> GameResult {
        match player.cmp(comp) {
            Ordering::Greater => GameResult::Victory,
            Ordering::Equal => GameResult::Draw,
            _ => GameResult::Loss
        }
    }
}

#[derive(Default)]
pub struct SessionRecord {
    wins: u32,
    losses: u32,
    draws: u32
}

impl fmt::Display for SessionRecord {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "W: {}\nL: {}\nD: {}",
            self.wins,
            self.losses,
            self.draws)
    }
}

impl SessionRecord {
    pub fn update_record(&mut self, latest_result: &GameResult) {
        match latest_result {
            GameResult::Victory => self.wins += 1,
            GameResult::Loss => self.losses += 1,
            GameResult::Draw => self.draws += 1
        }
    }
}
